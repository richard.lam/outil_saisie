#!/bin/bash

# L’option -p (prompt) permet d'imprimée avant l'exécution et n'inclut pas de nouvelle ligne

read -p "Veuillez saisir votre nom" nom
read -p "Veuillez saisir votre prénom" prenom
read -p "Veuillez saisir votre note" note

# On crée une condition avec if pour chaque note saisie et on affiche les variables dans le echo

if [ $note -eq 20 ]; then
    echo "$nom;$prenom;$note;Parfait"

elif [ $note -ge 18 ]; then
    echo "$nom;$prenom;$note;Excellent"

elif [ $note -ge 16 ]; then 
    echo "$nom;$prenom;$note;Très bien"

elif [ $note -ge 14 ]; then
    echo "$nom;$prenom;$note;Bien"

elif [ $note -ge 12 ]; then
    echo "$nom;$prenom;$note;Assez bien" 

elif [ $note -lt 10 ]; then
    echo "$nom;$prenom;$note;Insuffisant"

else 
    echo "$nom;$prenom;$note;Veuillez saisir une note"

fi