#!/bin/bash

# L’option -p (prompt) permet d'imprimée avant l'exécution et n'inclut pas de nouvelle ligne

read -p "Veuillez saisir une note" note 

# On crée une condition avec if pour chaque note saisie

if [ $note -eq 20 ]; then
    echo "Parfait"

elif [ $note -ge 18 ]; then
    echo "Excellent"

elif [ $note -ge 16 ]; then 
    echo "Très bien"

elif [ $note -ge 14 ]; then
    echo "Bien"

elif [ $note -ge 12 ]; then
    echo "Assez bien" 

elif [ $note -lt 10 ]; then
    echo "Insuffisant"

else 
    echo "Veuillez saisir une note"

fi

