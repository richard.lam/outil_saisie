#!/bin/bash

# # L’option -p (prompt) permet d'imprimée avant l'exécution et n'inclut pas de nouvelle ligne

read -p "Veuillez saisir votre nom de classe" nomdelaclasse
read -p "Veuillez saisir votre nombre d'élèves" nombreeleves

# Si i est inférieur à nombreeeleves à chaque itération, on continue de boucler. Dès que i dépasse nombreeleves, la boucle s'arrête

for ((i=0;i<nombreeleves;i++))
do read -p "Veuillez saisir votre nom" nom
   read -p "Veuillez saisir votre prenom" prenom
   read -p "Veuillez saisir votre note" note

if [ $note -eq 20 ]; then
    echo "$nom;$prenom;$note;Parfait"

elif [ $note -ge 18 ]; then
    echo "$nom;$prenom;$note;Excellent"

elif [ $note -ge 16 ]; then 
    echo "$nom;$prenom$note;Très bien"

elif [ $note -ge 14 ]; then
    echo "$nom;$prenom;$note;Bien"

elif [ $note -ge 12 ]; then
    echo "$nom;$prenom;$note;Assez bien" 

elif [ $note -lt 10 ]; then
    echo "$nom;$prenom;$note;Insuffisant"

else 
    echo "$nom;$prenom;$note;Veuillez saisir une note"

fi
done